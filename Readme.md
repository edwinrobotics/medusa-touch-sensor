# Medusa Touch Sensor

![Medusa Touch Sensor](https://shop.edwinrobotics.com/4060-thickbox_default/medusa-touch-sensor.jpg "Medusa Touch Sensor")

[Medusa Touch Sensor](https://shop.edwinrobotics.com/modules/1212-medusa-touch-sensor.html)


This easy to plug in touch sensor uses TTP223-B touch detector IC which detects change in capacitance when the metallic pad is touched. This should be placed on non metallic surface. Response time of max 60mS at fast mode, 220mS at low power mode @VDD=3V. Solder jumpers on the back allows toggling between momentary and toggle mode.


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.